package com.chandu0101.passwordmemory.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by chandrasekharkode on 3/23/14.
 */
public class HashGenerator {

    /*
    * This method will generate SHA 256 hash for given string
    * */
    public static String generateHash(String input)  {
        MessageDigest md = null;
        StringBuffer sb = new StringBuffer();
        try {
            md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes());
            byte byteData[] = md.digest();
            //convert the byte to hex format method 1
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
        } catch (NoSuchAlgorithmException e) { // lazy
            e.printStackTrace();
        }
        return sb.toString();
    }


}
