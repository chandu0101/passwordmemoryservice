package com.chandu0101.passwordmemory.entity;

import com.mongodb.DBObject;

/**
 * Created by chandrasekharkode on 12/17/13.
 */


public abstract class BaseEntity {

    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public abstract DBObject toDBObject();

}
