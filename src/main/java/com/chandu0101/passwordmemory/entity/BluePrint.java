package com.chandu0101.passwordmemory.entity;

import com.chandu0101.passwordmemory.util.StringUtils;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.util.Map;

import static com.chandu0101.passwordmemory.util.StringUtils.hasText;

/**
 * Created by chandrasekharkode on 12/15/13.
 */

public class BluePrint extends BaseEntity {


    public static final String USERNAME = "username";
    public static final String URL = "url";
    public static final String PASSWORD = "password";
    public static final String ID = "_id";

    private String url;
    private String username;
    private String password;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public DBObject toDBObject() {
        DBObject dbObject = new BasicDBObject();
        dbObject.put(URL, getUrl());
        dbObject.put(USERNAME, getUsername());
        dbObject.put(PASSWORD, getPassword());
        return dbObject;
    }

    public static BluePrint fromDBObject(DBObject dbObject) {
        BasicDBObject basicDBObject = (BasicDBObject) dbObject;
        BluePrint bluePrint = new BluePrint();
        bluePrint.setId(basicDBObject.getObjectId(ID).toString());
        bluePrint.url = basicDBObject.getString(URL);
        bluePrint.username = basicDBObject.getString(USERNAME);
        bluePrint.password = basicDBObject.getString(PASSWORD);
        return bluePrint;
    }

    public static DBObject createQuery(Map<String,String> map) {
        DBObject query = new BasicDBObject();
        if(hasText(map.get(URL))) {
            query.put(URL,map.get(URL));
        }
        if(hasText(map.get(USERNAME))) {
            query.put(USERNAME,map.get(USERNAME));
        }
        if(hasText(map.get(PASSWORD))) {
            query.put(PASSWORD,map.get(PASSWORD));
        }
        return  query;
    }

    @Override
    public String toString() {
        return "BluePrint{" +
                "id='" + getId() + '\'' +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
