package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.util.HashGenerator;

import javax.annotation.security.PermitAll;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;

import static com.chandu0101.passwordmemory.filter.SecurityFilter.AUTH_TOKEN;
import static com.chandu0101.passwordmemory.filter.SecurityFilter.HASH_VALUE;

/**
 * Created by chandrasekharkode on 3/24/14.
 */

@Path(Link.AUTHENTICATE)
public class LoginController {

     @GET
     @Path("{masterkey}")
     @PermitAll
     @Consumes("application/json")
     public Response verifyLogin(@PathParam("masterkey")String masterkey) {
         if(!HASH_VALUE.equals(HashGenerator.generateHash(masterkey))){
             throw  new NotAuthorizedException("Not Authorized");
         }
         HashMap<String,String> map = new HashMap<>();
         map.put(AUTH_TOKEN, HASH_VALUE);
        return Response.ok(map, MediaType.APPLICATION_JSON).build();
    }


}
