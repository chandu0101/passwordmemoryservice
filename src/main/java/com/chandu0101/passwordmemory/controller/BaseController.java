package com.chandu0101.passwordmemory.controller;

import javax.ws.rs.core.Response;
import java.net.URI;

/**
 * Created by chandrasekharkode on 12/18/13.
 */
public abstract class BaseController {

    protected Response created(Link resource) {
        String href = (String)resource.get("href");
        URI uri = URI.create(href);
        return Response.created(uri).entity(resource).build();
    }

}
