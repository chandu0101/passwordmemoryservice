package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.entity.BluePrint;
import com.chandu0101.passwordmemory.service.MemoryReadWrite;
import com.chandu0101.passwordmemory.util.StringUtils;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.*;

import static com.chandu0101.passwordmemory.util.StringUtils.hasText;

/**
 * Created by chandrasekharkode on 12/15/13.
 */

@Path(Link.EPASSWORDS)
public class PasswordMemoryController extends BaseController {

    public static final String URL = "url";
    public static final String USERNAME = "username";
    @Inject
    MemoryReadWrite memoryReadWrite;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CollectionResource readAll(@Context UriInfo info,@DefaultValue("true") @QueryParam("expand") boolean expand) {

        Collection<BluePrint> allEPass = memoryReadWrite.readAll();
        if(allEPass == null || allEPass.size() == 0) {
            return new CollectionResource(info,Link.EPASSWORDS, Collections.emptyList());
        }

        Collection<? super Link> items = new ArrayList<>(allEPass.size());
        for(BluePrint bluePrint :allEPass) {
            if(expand) {
                items.add(new BluePrintResource(info,bluePrint));
            } else {
                items.add(new Link(info,bluePrint));
            }
        }
        return  new CollectionResource(info,Link.EPASSWORDS,items);
    }

    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BluePrintResource read(@Context UriInfo info,@PathParam("id") String id) {
        BluePrint bluePrint = memoryReadWrite.read(id);
        if(bluePrint == null) {
            throw new UnknownResourceException();
        }
        return new BluePrintResource(info,bluePrint);
    }

    @Path("findBy")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public BluePrintResource readByUrlAndUsername(@Context UriInfo info,@QueryParam(URL) String url,@QueryParam(USERNAME) String username) {
        BluePrint bluePrint = null;
        if(hasText(url) || hasText(username)) {
          Map map = new HashMap();
          map.put(URL,url);
          map.put(USERNAME,username);
          bluePrint = memoryReadWrite.readByQuery(BluePrint.createQuery(map));
        } else {
            throw new IllegalArgumentException();
        }
        return new BluePrintResource(info,bluePrint);
    }

    @POST
    @Consumes("application/json")
    public Response  write(@Context UriInfo info,BluePrint bluePrint) {
          BluePrint savedBP = memoryReadWrite.write(bluePrint);
         return created(new BluePrintResource(info,savedBP));
    }

    @Path("/{id}")
    @PUT
    @Consumes("application/json")
    public Response update(@Context UriInfo info,@PathParam("id") String id,Map newValues) {
        BluePrint bluePrint = memoryReadWrite.read(id);
        if(bluePrint == null) {
            throw new UnknownResourceException();
        }
        if(newValues.containsKey(URL)) {
            bluePrint.setUrl(String.valueOf(newValues.get("url")));
        }
        if(newValues.containsKey(USERNAME)) {
            bluePrint.setUsername(String.valueOf(newValues.get(USERNAME)));
        }
        if(newValues.containsKey("encryptedPassword")) {
            bluePrint.setPassword(String.valueOf(newValues.get("encryptedPassword")));
        }

        memoryReadWrite.update(bluePrint);

        return created(new BluePrintResource(info,bluePrint));

    }

    @Path("/{id}")
    @DELETE
    @Consumes("application/json")
    public void erase(@Context UriInfo info , @PathParam("id") String id) {
        BluePrint bluePrint = memoryReadWrite.read(id);
        if(bluePrint == null) {
            throw new UnknownResourceException();
        }
        memoryReadWrite.erase(id);
    }




}

