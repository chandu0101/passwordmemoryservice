package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.entity.BaseEntity;
import com.chandu0101.passwordmemory.entity.BluePrint;

/**
 * Created by chandrasekharkode on 12/17/13.
 *
 */
public enum ResourcePath {

    pms(Link.EPASSWORDS, BluePrint.class);

    final String path;
    final Class<? extends BaseEntity> associatedClass;
    ResourcePath(String path, Class<? extends BaseEntity> aClass) {
       this.path = path;
       associatedClass = aClass;
    }

    public String getPath() {
        return path;
    }

    public Class<? extends BaseEntity> getAssociatedClass() {
        return associatedClass;
    }

    public static ResourcePath forClass(Class<? extends BaseEntity> clazz) {
        for (ResourcePath rp : values()) {
            //Cannot use equals because of hibernate proxied object
            //Cannot use instanceof because type not fixed at compile time
            if (rp.associatedClass.isAssignableFrom(clazz)) {
                return rp;
            }
        }
        throw new IllegalArgumentException("No ResourcePath for class '" + clazz.getName() + "'");
    }
}
