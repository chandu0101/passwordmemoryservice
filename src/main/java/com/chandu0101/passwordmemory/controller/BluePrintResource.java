package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.entity.BluePrint;

import javax.ws.rs.core.UriInfo;

import java.util.Map;

import static com.chandu0101.passwordmemory.entity.BluePrint.*;

/**
 * Created by chandrasekharkode on 12/18/13.
 */
public class BluePrintResource extends Link {


    public static final String HREF = "href";

    public BluePrintResource(){}

    public BluePrintResource(UriInfo info, BluePrint bluePrint) {
        super(info, bluePrint);
        put(URL,bluePrint.getUrl());
        put(USERNAME,bluePrint.getUsername());
        put(PASSWORD,bluePrint.getPassword());
    }

    public BluePrint toBluePrint() {
        BluePrint result = new BluePrint();
        result.setUrl(get(URL).toString());
        result.setUsername(get(USERNAME).toString());
        result.setPassword(get(PASSWORD).toString());
        result.setId(getHref().toString().substring(getHref().lastIndexOf('/')));
        return result;
    }

    public static BluePrint toBluePrint(Map map) {
        BluePrint result = new BluePrint();
        result.setUrl(map.get(URL).toString());
        result.setUsername(map.get(USERNAME).toString());
        result.setPassword(map.get(PASSWORD).toString());
        result.setId(map.get(HREF).toString().substring(map.get(HREF).toString().lastIndexOf('/')));
        return result;
    }
}
