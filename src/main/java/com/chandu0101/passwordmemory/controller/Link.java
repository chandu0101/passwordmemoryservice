package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.entity.BaseEntity;

import javax.ws.rs.core.UriInfo;
import java.util.LinkedHashMap;

/**
 * Created by chandrasekharkode on 12/17/13.
 */
public class Link extends LinkedHashMap {

 public static final String PATH_SEPARATOR = "/";
 public static final String EPASSWORDS = PATH_SEPARATOR + "epasswords";
 public static  final String AUTHENTICATE = PATH_SEPARATOR + "authenticate";

    public Link(){}

    public Link(UriInfo info, BaseEntity entity) {
        this(getFullyQualifiedContextPath(info), entity);
    }

    public Link(String fqBasePath, BaseEntity entity) {
        String href = createHref(fqBasePath, entity);
        put("href", href);
    }

    public Link(UriInfo info, String subPath) {
        this(getFullyQualifiedContextPath(info), subPath);
    }

    public Link(String fqBasePath, String subPath) {
        String href = fqBasePath + subPath;
        put("href", href);
    }

    protected static String getFullyQualifiedContextPath(UriInfo info) {
        String fq = info.getBaseUri().toString();
        if (fq.endsWith("/")) {
            return fq.substring(0, fq.length()-1);
        }
        return fq;
    }

    protected String createHref(String fqBasePath, BaseEntity entity) {
        StringBuilder sb = new StringBuilder(fqBasePath);
        ResourcePath path = ResourcePath.forClass(entity.getClass());
        sb.append(path.getPath()).append(PATH_SEPARATOR).append(entity.getId());
        return sb.toString();
    }

    public String getHref() {
        return (String)get("href");
    }

}
