
package com.chandu0101.passwordmemory;

import com.chandu0101.passwordmemory.controller.LoginController;
import com.chandu0101.passwordmemory.controller.PasswordMemoryController;
import com.chandu0101.passwordmemory.exception.DefaultExceptionMapper;
import com.chandu0101.passwordmemory.filter.SecurityFilter;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by chandrasekharkode on 12/15/13.
 */


@ApplicationPath(App.API_PATH)
public class App  extends Application {

    public static final String  API_PATH = "api";
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<Class<?>>();
        classes.add(SecurityFilter.class);
        classes.add(PasswordMemoryController.class);
        classes.add(LoginController.class);
        classes.add(DefaultExceptionMapper.class);
        return classes;
    }


}
