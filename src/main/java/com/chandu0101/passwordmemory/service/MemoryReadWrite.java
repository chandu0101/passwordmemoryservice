package com.chandu0101.passwordmemory.service;

import com.chandu0101.passwordmemory.entity.BluePrint;
import com.mongodb.*;
import org.bson.types.ObjectId;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by chandrasekharkode on 12/15/13.
 */
@Transactional
public class MemoryReadWrite {

    private static final String BLUEPRINTS = "blueprints";

    DBCollection blueprintCollection;

    /*
    * Initiates MongoDB connection
    * */
    @PostConstruct
    public void initDB(){
        try {
            MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
            DB db = mongoClient.getDB("test");
            blueprintCollection  = db.getCollection(BLUEPRINTS);
            if (!db.collectionExists(BLUEPRINTS)) {
                blueprintCollection = db.createCollection(BLUEPRINTS, null);
                blueprintCollection.ensureIndex(new BasicDBObject("url",1).append("username", 1),"B_UNIQUE_INDEX",true);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }

    public Collection<BluePrint> readAll() {
        Collection<BluePrint> allBluePrints = new ArrayList<>();
        DBCursor cursor = null;
        try {
             cursor = blueprintCollection.find();
            for(DBObject dbObject :cursor.toArray()) {
                allBluePrints.add(BluePrint.fromDBObject(dbObject));
            }
        } finally {
            cursor.close();
        }
        return  allBluePrints;
    }

    public BluePrint read(String id) {
        DBObject searchById = new BasicDBObject("_id",new ObjectId(id));
        DBObject dbObject = blueprintCollection.findOne(searchById);
         return  BluePrint.fromDBObject(dbObject);
    }

    public BluePrint write(BluePrint bluePrint) {
        DBObject dbObject = bluePrint.toDBObject();
        blueprintCollection.save(dbObject);
        return BluePrint.fromDBObject(dbObject);
    }


    public BluePrint update(BluePrint bluePrint) {
        DBObject searchById = new BasicDBObject("_id",new ObjectId(bluePrint.getId()));
        blueprintCollection.update(searchById,bluePrint.toDBObject());
        return  bluePrint;
    }

    public void erase(String id) {
       blueprintCollection.remove(new BasicDBObject("_id",new ObjectId(id)));
    }

    public BluePrint readByQuery(DBObject query) {
        DBObject dbObject = blueprintCollection.findOne(query);
        return BluePrint.fromDBObject(dbObject);
    }

}
