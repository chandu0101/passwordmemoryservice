package com.chandu0101.passwordmemory.filter;

import org.jboss.resteasy.core.ResourceMethodInvoker;

import javax.annotation.Priority;
import javax.annotation.security.PermitAll;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Created by chandrasekharkode on 3/23/14.
 */

@Provider
@Priority(Priorities.AUTHENTICATION)
public class SecurityFilter implements ContainerRequestFilter {
    public static final String HASH_VALUE  = "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08"; //test
    public static final String AUTH_TOKEN = "authToken";

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {
        ResourceMethodInvoker methodInvoker = (ResourceMethodInvoker) containerRequestContext.getProperty("org.jboss.resteasy.core.ResourceMethodInvoker");
        Method method = methodInvoker.getMethod();

        if (!method.isAnnotationPresent(PermitAll.class)) {
            MultivaluedMap<String, String> headers = containerRequestContext.getHeaders();
            if(!headers.containsKey(AUTH_TOKEN) || !HASH_VALUE.equalsIgnoreCase(headers.getFirst(AUTH_TOKEN)) ) {
                throw new NotAuthorizedException("Not Authorized");
            }
        }

    }
}
