package com.chandu0101.passwordmemory;

import com.chandu0101.passwordmemory.controller.BaseController;
import com.chandu0101.passwordmemory.entity.BluePrint;
import com.chandu0101.passwordmemory.error.RestError;
import com.chandu0101.passwordmemory.exception.DefaultExceptionMapper;
import com.chandu0101.passwordmemory.filter.SecurityFilter;
import com.chandu0101.passwordmemory.http.HttpStatus;
import com.chandu0101.passwordmemory.service.MemoryReadWrite;
import com.chandu0101.passwordmemory.util.ClassUtils;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;

/**
 * Created by chandrasekharkode on 3/30/14.
 */
public class TestDeployment {


    private WebArchive webArchive;

    public TestDeployment() {
        webArchive = ShrinkWrap.create(WebArchive.class, "test.war").
                addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    public TestDeployment withPersistence() {
        webArchive = webArchive.addPackage(BluePrint.class.getPackage()).addPackage(MemoryReadWrite.class.getPackage())
                               .addAsLibraries(Maven.resolver().resolve("org.mongodb:mongo-java-driver:2.11.4").withTransitivity().asFile());
        return this;
    }

    public TestDeployment withRestful() {
        webArchive = webArchive.addPackage(BaseController.class.getPackage())
                .addPackage(HttpStatus.class.getPackage())
                .addPackage(RestError.class.getPackage())
                .addPackage(DefaultExceptionMapper.class.getPackage())
                .addAsResource("restErrors.properties")
                .addClass(SecurityFilter.class)
                .addPackage(ClassUtils.class.getPackage())
                .addClass(App.class);
        return this;
    }

    public WebArchive getArchive() {
        return webArchive;
    }
}
