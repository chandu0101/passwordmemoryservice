package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.TestDeployment;
import com.chandu0101.passwordmemory.entity.BluePrint;
import com.chandu0101.passwordmemory.filter.ClientAuthFilter;
import com.chandu0101.passwordmemory.filter.SecurityFilter;
import com.chandu0101.passwordmemory.http.HttpStatus;
import com.chandu0101.passwordmemory.service.MemoryReadWrite;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.*;

import static com.chandu0101.passwordmemory.App.API_PATH;
import static com.chandu0101.passwordmemory.controller.BluePrintResource.toBluePrint;
import static com.chandu0101.passwordmemory.controller.Link.EPASSWORDS;
import static com.chandu0101.passwordmemory.filter.SecurityFilter.AUTH_TOKEN;
import static com.chandu0101.passwordmemory.filter.SecurityFilter.HASH_VALUE;
import static com.chandu0101.passwordmemory.http.HttpStatus.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by chandrasekharkode on 4/12/14.
 */

@RunWith(Arquillian.class)
public class PasswordMemoryControllerTest {


    public static final String PASSMCURL_1 = "passmcurl1";
    public static final String PASSMCUSERNAME_1 = "passmcusername1";
    public static final String PASSMCPASSWORD1 = " passmcpassword1";
    public static final String URL = "url";
    public static final String USERNAME = "username";
    public static final String NEW = "NEW";


    @Deployment(testable = false) // testable = false here means all the tests are running outside of the container
    public static WebArchive create() {
        WebArchive war = new TestDeployment().withPersistence().withRestful().getArchive();
        System.out.println(war.toString(true));
        return war;
    }

    private WebTarget target;

    @ArquillianResource
    private URL base;


    @Before
    public void setUpClass() throws MalformedURLException {
        Client client = ClientBuilder.newClient().register(ClientAuthFilter.class);
        target = client.target(URI.create(new URL(base, API_PATH.concat(EPASSWORDS)).toExternalForm()));
    }


    @Test
    @InSequence(1)
    public void testWrite() throws Exception {
        BluePrint bluePrint = new BluePrint();
        bluePrint.setUrl(PASSMCURL_1);
        bluePrint.setUsername(PASSMCUSERNAME_1);
        bluePrint.setPassword(PASSMCPASSWORD1);
        Response response = target.request(MediaType.APPLICATION_JSON).post(Entity.json(bluePrint));
        assertEquals("should return created resource response", CREATED.value(),response.getStatus());
        BluePrintResource bluePrintResource = response.readEntity(BluePrintResource.class);
        assertEquals("should return blue print with pass "+PASSMCPASSWORD1 ,PASSMCPASSWORD1, bluePrintResource.toBluePrint().getPassword());
    }

    @Test
    @InSequence(2)
    public void testReadAll() throws Exception {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertEquals("should return success response", OK.value(),response.getStatus());
        CollectionResource collectionResource = response.readEntity(CollectionResource.class);
        Collection<BluePrintResource> allBluePrintResources = (Collection<BluePrintResource>) collectionResource.get("items");
        assertTrue("should return values mßore than 1" , allBluePrintResources.size() >=1 );
    }

    @Test
    @InSequence(3)
    public void testReadByURLAndUsername() throws  Exception {
        BluePrintResource bluePrintResource = readByUrlAndUsername(PASSMCURL_1,PASSMCUSERNAME_1);
        assertEquals("should return resource with username and url",PASSMCPASSWORD1,bluePrintResource.toBluePrint().getPassword());

    }

    private BluePrintResource readByUrlAndUsername(String url,String username) {
        BluePrintResource bluePrintResource = null;
        Response response = target.path("findBy").queryParam(URL,url).queryParam(USERNAME,username).request(MediaType.APPLICATION_JSON).get();
        if(response.getStatus() == OK.value()) {
            bluePrintResource = response.readEntity(BluePrintResource.class);
        }
        return bluePrintResource;
    }

    @Test
    @InSequence(4)
    public void testRead() throws Exception {
        String id = readByUrlAndUsername(PASSMCURL_1,PASSMCUSERNAME_1).toBluePrint().getId();
        Response response = target.path(id).request(MediaType.APPLICATION_JSON).get();
        BluePrintResource bluePrintResource = response.readEntity(BluePrintResource.class);
       assertEquals("should return record for a given id",PASSMCPASSWORD1, bluePrintResource.toBluePrint().getPassword());
    }



    @Test
    @InSequence(5)
    public void testUpdate() throws Exception {
        String id = readByUrlAndUsername(PASSMCURL_1,PASSMCUSERNAME_1).toBluePrint().getId();
        Map map = new HashMap();
        map.put(URL,PASSMCURL_1.concat(NEW));
        Response  response = target.path(id).request(MediaType.APPLICATION_JSON).put(Entity.json(map));
        BluePrintResource bluePrintResource = response.readEntity(BluePrintResource.class);
        assertEquals("should update url " ,PASSMCURL_1.concat(NEW),bluePrintResource.toBluePrint().getUrl());

    }

    @Test
    @InSequence(6)
    public void testErase() throws Exception {
       BluePrintResource bluePrintResource = readByUrlAndUsername(PASSMCURL_1.concat(NEW),PASSMCUSERNAME_1);
       String id = bluePrintResource.toBluePrint().getId();
       Response response = target.path(id).request(MediaType.APPLICATION_JSON).delete();
       bluePrintResource =  readByUrlAndUsername(PASSMCURL_1.concat(NEW),PASSMCUSERNAME_1);
       assertNull("should return null for deleted object search",bluePrintResource);
    }
}
