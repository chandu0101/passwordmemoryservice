package com.chandu0101.passwordmemory.controller;

import com.chandu0101.passwordmemory.TestDeployment;
import com.chandu0101.passwordmemory.filter.SecurityFilter;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.chandu0101.passwordmemory.App.API_PATH;
import static com.chandu0101.passwordmemory.controller.Link.AUTHENTICATE;
import static com.chandu0101.passwordmemory.filter.SecurityFilter.AUTH_TOKEN;
import static org.junit.Assert.assertEquals;

/**
 * Created by chandrasekharkode on 3/26/14.
 */


@RunWith(Arquillian.class)
public class LoginControllerTest {

    @Deployment(testable = false) // testable = false here means all the tests are running outside of the container
    public static WebArchive create() {
        WebArchive war = new TestDeployment().withPersistence().withRestful().getArchive();
        System.out.println(war.toString(true));
        return war;
    }

    private WebTarget target;

    @ArquillianResource
    private URL base;

    @Before
    public void setUpClass() throws MalformedURLException {
        Client client = ClientBuilder.newClient();
        target = client.target(URI.create(new URL(base, API_PATH.concat(AUTHENTICATE)).toExternalForm()));
    }


    @Test
    public void testVerifyLogin() {
        Response response = target.path("test").request().get();
        Map<String, String> resultMap = response.readEntity(HashMap.class);
        assertEquals("Should return token for master password ", "9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08", resultMap.get(AUTH_TOKEN));

    }
}
