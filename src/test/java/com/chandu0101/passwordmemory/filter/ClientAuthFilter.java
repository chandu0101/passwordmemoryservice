package com.chandu0101.passwordmemory.filter;

import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import java.io.IOException;

import static com.chandu0101.passwordmemory.filter.SecurityFilter.AUTH_TOKEN;
import static com.chandu0101.passwordmemory.filter.SecurityFilter.HASH_VALUE;

/**
 * Created by chandrasekharkode on 4/20/14.
 */
public class ClientAuthFilter implements ClientRequestFilter {
    @Override
    public void filter(ClientRequestContext clientRequestContext) throws IOException {
        clientRequestContext.getHeaders().putSingle(AUTH_TOKEN,HASH_VALUE);
    }
}
