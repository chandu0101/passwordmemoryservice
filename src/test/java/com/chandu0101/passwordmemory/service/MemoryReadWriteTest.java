package com.chandu0101.passwordmemory.service;

import com.chandu0101.passwordmemory.TestDeployment;
import com.chandu0101.passwordmemory.entity.BluePrint;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by chandrasekharkode on 3/30/14.
 */

@RunWith(Arquillian.class)
public class MemoryReadWriteTest {

    public static final String TESTURL_1 = "testurl1";
    public static final String TESTUSER_1 = "testuser1";
    public static final String TESTUSER_2 = "testuser2";
    public static final String TESTURL_2 = "testurl2";
    public static final String TEST_PASSWORD_2 = "testPassword2";
    public static final String TEST_PASSWORD_1 = "testPassword1";
    @Inject
    private MemoryReadWrite memoryReadWrite;

    @Deployment
    public static WebArchive create() {
        WebArchive war = new TestDeployment().withPersistence().getArchive();
        System.out.println(war.toString(true));
        return war;
    }


    @Test
    @InSequence(1)
    public void testWrite() throws Exception {
        BluePrint record1 = new BluePrint();
        record1.setUsername(TESTUSER_1);
        record1.setUrl(TESTURL_1);
        record1.setPassword(TEST_PASSWORD_1);
        BluePrint saved = memoryReadWrite.write(record1);
        assertEquals("It should save record1 object in database",
                record1.getPassword(), saved.getPassword()
        );
    }


    @Test
    @InSequence(2)
    public void testReadByQuery() throws Exception {
        DBObject searchByUrlAndUsername = new BasicDBObject("url",TESTURL_1).append("username",TESTUSER_1);
        BluePrint bluePrint = memoryReadWrite.readByQuery(searchByUrlAndUsername);
        assertEquals("should return object with username and password",TEST_PASSWORD_1,bluePrint.getPassword());
    }

    @Test
    @InSequence(3)
    public void testUniqueIndex() {
        String message = null;
        try {
            BluePrint record1 = new BluePrint();
            record1.setUsername(TESTUSER_1);
            record1.setUrl(TESTURL_1);
            record1.setPassword(TEST_PASSWORD_1);
            memoryReadWrite.write(record1);
        } catch (Exception e) {
            message = e.getMessage();
        }
        assertTrue("should return duplicate key error", message.contains("duplicate key error"));
    }


    @Test
    @InSequence(4)
    public void testReadAll() throws Exception {

        BluePrint record2 = new BluePrint();
        record2.setUsername(TESTUSER_2);
        record2.setUrl(TESTURL_2);
        record2.setPassword(TEST_PASSWORD_2);
        memoryReadWrite.write(record2);
        Collection<BluePrint> allRecords = memoryReadWrite.readAll();
        System.out.println("size : " + allRecords.size());
        assertTrue("Should return atleast two records", allRecords.size() >= 2);
    }


    @Test
    @InSequence(5)
    public void testRead() throws Exception {
        DBObject searchByUsernameAndUrl = new BasicDBObject("url", TESTURL_1).append("username", TESTUSER_1);
        BluePrint bluePrint = memoryReadWrite.readByQuery(searchByUsernameAndUrl);
        BluePrint record = memoryReadWrite.read(bluePrint.getId());
        assertEquals("Should return record with id ", TEST_PASSWORD_1, record.getPassword());
    }


    @Test
    @InSequence(7)
    public void testErase() throws Exception {
        Collection<BluePrint> allRecords = memoryReadWrite.readAll();
        int orginalSize = allRecords.size();
        for (BluePrint record : allRecords) {
            if (TESTURL_1.equalsIgnoreCase(record.getUrl()) || TESTURL_2.equalsIgnoreCase(record.getUrl())) {
                memoryReadWrite.erase(record.getId());
            }
        }
        assertEquals("should delete two records from table", memoryReadWrite.readAll().size(), orginalSize - 2);
    }


}
